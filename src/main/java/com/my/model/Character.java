package com.my.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Character {

    @Column(name = "name", nullable = false)
    private String name;

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Integer id;
    
    public Character() {
    }
    
    public Character(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public Integer getId() {
      return id;
    }

    public void setId(Integer id) {
      this.id = id;
    }
}