
package com.my.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.my.model.Character;

public interface CharacterRepository extends JpaRepository<Character, Integer> {
}