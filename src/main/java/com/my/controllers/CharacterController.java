
package com.my.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.my.model.Character;
import com.my.repositories.CharacterRepository;

@RestController
class CharacterController {
 
    @Autowired
    private CharacterRepository repository;
 
    @RequestMapping("/characters")
    List<Character> characters() {
        return repository.findAll();
    }
    
    @RequestMapping(value = "/characters/{id}")
    Character get(@PathVariable("id") Integer id) {
        return repository.findOne(id);
    }
    
    @RequestMapping(value = "/characters", method = RequestMethod.POST)
    Character post(@RequestBody Character ch) {
        return repository.save(ch);
    }
 
    @RequestMapping(value = "/characters/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@PathVariable("id") Integer id) {
        repository.delete(id);
    }
}